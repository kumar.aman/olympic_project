package com.aman.olympics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {
    private static final int ATHLETE_ID = 0;
    private static final int NAME = 1;
    private static final int SEX = 2;
    private static final int AGE = 3;
    private static final int EVENT = 13;
    private static final int TEAM = 6;
    private static final int SEASON = 10;
    private static final int SPORT = 12;
    private static final int YEAR = 9;
    private static final int MEDAL =14 ;

    public static void main(String[] args) {
        List<Athlete> athletes = getAthleteData();

        displayNumberOfGoldMedalWonByPlayerYearWise(athletes);
        detailsOfAthleteWhoWonGoldIn1980WithAgeLessThan30(athletes);
        findNumberOfMedalInYear1980EventWise(athletes);
        displayGoldWinnerInFootballEachOlympics(athletes);
        displayFemaleWithMaxNoOfGold(athletes);
    }

    private static void displayFemaleWithMaxNoOfGold(List<Athlete> athletes) {
        Map<String,Integer> femaleGoldWinner= new HashMap<>();
        int noOfGold;
        for(Athlete athlete:athletes){
            if(athlete.getMedal().equals("\"Gold\"") && athlete.getSex().equals("\"F\"")){
                if(femaleGoldWinner.containsKey(athlete.getName())){
                    noOfGold=femaleGoldWinner.get(athlete.getName())+1;
                    femaleGoldWinner.put(athlete.getName(),noOfGold);
                }else {
                    femaleGoldWinner.put(athlete.getName(), 1);
                }
            }
        }
        String player ="";
        int max = 0;
        for(Map.Entry<String,Integer> entry:femaleGoldWinner.entrySet()){

            if(max < entry.getValue()){
                max= entry.getValue();
                player=entry.getKey();
            }
        }
        System.out.println(player+"have the maximum "+max+" gold medal in female");

    }

    private static void displayGoldWinnerInFootballEachOlympics(List<Athlete> athletes) {
        HashSet<String> goldWinnerFootballYearWise = new HashSet<>();
        for (Athlete athlete:athletes) {
            if(athlete.getMedal().equals("\"Gold\"")&&athlete.getSport().equals("\"Football\""))
            goldWinnerFootballYearWise.add(athlete.getYear()+""+athlete.getEvent()+""+athlete.getTeam());
        }
        Iterator<String> itr=goldWinnerFootballYearWise.iterator();
        while (itr.hasNext()){
            System.out.println(itr.next());
        }
    }

    private static void findNumberOfMedalInYear1980EventWise(List<Athlete> athletes) {
        Map<String,HashMap<String,Integer>> numberOfMedalIn1980 = new HashMap<>();
        for(Athlete athlete:athletes){
            if(athlete.getYear().equals("1980") && !athlete.getMedal().equals("NA")){
                if(numberOfMedalIn1980.containsKey(athlete.getEvent())){
                    if(numberOfMedalIn1980.get(athlete.getEvent()).containsKey(athlete.getMedal())){
                        int no = numberOfMedalIn1980.get(athlete.getEvent()).get(athlete.getMedal())+1;
                        numberOfMedalIn1980.get(athlete.getEvent()).put(athlete.getMedal(), no);
                    }else{
                        numberOfMedalIn1980.get(athlete.getEvent()).put(athlete.getMedal(), 1);
                    }
                }else {
                    HashMap<String,Integer> temp = new HashMap<>();
                    temp.put(athlete.getMedal(),1);
                    numberOfMedalIn1980.put(athlete.getEvent(), temp );
                }
            }
        }
        for (Map.Entry<String,HashMap<String,Integer>> entry:numberOfMedalIn1980.entrySet()){
            //System.out.println(entry.getKey());
            for(Map.Entry<String,Integer> entry1:entry.getValue().entrySet()){
                System.out.println(entry.getKey()+" "+entry1.getKey()+" "+entry1.getValue());
            }
        }
    }

    private static void detailsOfAthleteWhoWonGoldIn1980WithAgeLessThan30(List<Athlete> athletes) {
        Map<String,String> athleteWonGold = new HashMap<>();
        int noOfGoldMedal = 0;
        for (Athlete athlete: athletes){
            if (athlete.getYear().equals("1980")&& athlete.getMedal().equals("\"Gold\"") && Integer.parseInt(athlete.getAge())<30){
                    if(athleteWonGold.containsKey((athlete.getId()))){
                        noOfGoldMedal = noOfGoldMedal+1;
                        athleteWonGold.put((athlete.getId()),athlete.getName()+" age: "+Integer.parseInt(athlete.getAge())+"  , no of gold:"+noOfGoldMedal);
                    }
                    else{
                        noOfGoldMedal=1;
                        athleteWonGold.put((athlete.getId()),athlete.getName()+" age: "+Integer.parseInt(athlete.getAge())+" , no of gold:"+noOfGoldMedal);
                    }
            }
        }
        for (Map.Entry<String,String>entry: athleteWonGold.entrySet()){
            System.out.println(entry.getKey()+" "+entry.getValue());
        }
    }

    private static void displayNumberOfGoldMedalWonByPlayerYearWise(List<Athlete> athletes) {
        Map<String,Integer> medalWonByPlayer= new HashMap<>();
        int noOfGold;
        for(Athlete athlete:athletes){
            if(!athlete.getMedal().equals("\"Gold\"")){
                continue;
            }
            if(medalWonByPlayer.containsKey(athlete.getName()+""+athlete.getYear())){
                noOfGold = medalWonByPlayer.get(athlete.getName()+""+athlete.getYear())+1;
                medalWonByPlayer.put(athlete.getName()+""+athlete.getYear(), noOfGold);

            }else{
                medalWonByPlayer.put(athlete.getName()+""+athlete.getYear(),1);
            }
            for (Map.Entry<String,Integer> entry: medalWonByPlayer.entrySet()){
                System.out.println(entry.getKey()+" no of gold medal won :"+entry.getValue());

            }




        }
    }

    private static List<Athlete> getAthleteData() {
        List<Athlete> athletes = new ArrayList<>();
        String csvFile = "/home/aman/Downloads/testing/csvFiles/athlete_events.csv";
        String line;
        int skip = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(csvFile));
            while ((line= br.readLine()) != null){
                if (skip == 0) {
                    skip++;
                }else {
                    String[] data = line.split(",");
                    Athlete athlete = new Athlete();
                   athlete.setId((data[ATHLETE_ID]));
                    athlete.setName(data[NAME]);
                    athlete.setSex(data[SEX]);
                    athlete.setAge((data[AGE]));
                    athlete.setEvent(data[EVENT]);
                    athlete.setTeam(data[TEAM]);
                    athlete.setSeason(data[SEASON]);
                    athlete.setSport(data[SPORT]);
                    athlete.setYear((data[YEAR]));
                    athlete.setMedal(data[MEDAL]);
                    athletes.add(athlete);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return athletes;
    }
}
